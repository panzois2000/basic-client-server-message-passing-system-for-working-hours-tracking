#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>
#include <string.h>
#include "project2.h"

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3


#define TAG 0
#define CONNECT_M 0
#define NEIGHBOR_M 1
#define ACK_M 2
#define CONN_END_M 3
#define START_LEADER_ELECTION_CL 4
#define ELECT 5
#define LEADER_ELECTION_CLIENT_DONE 6
#define START_LEADER_ELECTION_SE 7
#define LEADER 8
#define PARENT 9
#define ALREADY 10
#define LEADER_ANNOUNCEMENT 11
#define REGISTER_M 12
#define ACK_REGISTER 13
#define REGISTER_END 14
#define SYNC 15
#define SYNC_ACK 16
#define OVERTIME 17
#define OVERTIME_ACK 18
#define PRINT 19
#define PRINT_ACK 20


int main(int argc, char *argv[]){

	int rank, process_num, i;
	/** MPI Initialisation **/
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &process_num);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status;

	/** Create custom datatypes to send array of MPI_INTs **/
	MPI_Datatype CONN_MESSAGE;
	MPI_Type_contiguous(4, MPI_INT, &CONN_MESSAGE);
	MPI_Type_commit(&CONN_MESSAGE);

	MPI_Datatype ELECT_CLIENTS_MESSAGE;
	MPI_Type_contiguous(4, MPI_INT, &ELECT_CLIENTS_MESSAGE);
	MPI_Type_commit(&ELECT_CLIENTS_MESSAGE);

	MPI_Datatype ELECT_SERVERS_MESSAGE;
	MPI_Type_contiguous(4, MPI_INT, &ELECT_SERVERS_MESSAGE);
	MPI_Type_commit(&ELECT_SERVERS_MESSAGE);

	MPI_Datatype REGISTER_MESSAGE;
	MPI_Type_contiguous(30, MPI_CHAR, &REGISTER_MESSAGE);
	MPI_Type_commit(&REGISTER_MESSAGE);

	int num_servers = atoi(argv[1]);
	int connect_data[4];
	char register_data[6][30];
	int client_leader;
	int server_leader;
	/*Process is the coordinator*/
	if(rank == 0){

		// Coordinator
		printf("[rank: %d] Coordinator started\n", rank);

		FILE * fp;
		char * line = NULL;
		size_t len = 0;
		size_t read;

		fp = fopen(argv[2], "r");
		if(fp == NULL){
			printf("Wrong file! Try again!\n");
			exit(-1);
		}
		int registers = 0;
		int line_num = 1;
		char* command = NULL;
		char loadings[4] = {'|','/', '-', '\\'};
		int loading=0;
		while((read = getline(&line, &len, fp)) != -1){

			// printf("[%d]: %s", line_num, line);
			if(line[0]!='R'){
				printf("Processing %c",loadings[loading]);
				fflush(stdout);
				printf("\r");
				loading++;
				loading=loading%4;
			}
			line_num++;
			command=NULL;
			command = strtok(line, " ");
			for(int i =0; i<=strlen(command); i++){
				if((int)command[i]==10){
					command[i]='\0';
				}
			}
			if((int)command[strlen(command)-1]==13){
				command[strlen(command)-1]='\0';
			}
			if(strcmp(command, "CONNECT") == 0){
				/*Send message to a and wait for answer before proceeding to the next statement in file*/
				char* a = strtok(NULL, " ");
				char* b = strtok(NULL, " ");
				connect_data[0]=CONNECT_M;
				connect_data[1]=0;
				connect_data[2]=atoi(a);
				connect_data[3]=atoi(b);
				MPI_Send(&connect_data, 1, CONN_MESSAGE, atoi(a), TAG, MPI_COMM_WORLD);
				// printf("CONNECT SENT\n");
				MPI_Recv(&connect_data, 1, CONN_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);

			}else if(strcmp(command, "REGISTER") == 0){
				registers++;
				char* client_id = strtok(NULL, " ");
                char* type = strtok(NULL, " ");
				char* time = strtok(NULL, " ");
                char* date = strtok(NULL, " ");
				char timestamp[30];
				strcpy(timestamp, time);
				strcat(timestamp, " ");
				strcat(timestamp, date);
				timestamp[strlen(timestamp)-1] = '\0';
				sprintf(register_data[0],"%d", REGISTER_M);
				strcpy(register_data[1],client_id);
				strcpy(register_data[2],"0");
				strcpy(register_data[3],client_id);
				strcpy(register_data[4],type);
				strcpy(register_data[5],timestamp);
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				// printf("REGISTER SENT\n");
			}else if(strcmp(command, "OVERTIME") == 0){

				char* id = strtok(NULL, " ");
                char* timestamp = strtok(NULL, " ");
				// timestamp[strlen(timestamp)-1] = '\0';
				// printf("%d\n",strlen(timestamp));
				/*Send overtime to the server with the specific id*/
				sprintf(register_data[0],"%d", OVERTIME);
				sprintf(register_data[1],"%d", atoi(id));
				sprintf(register_data[2],"%d", rank);
				sprintf(register_data[3],"%d", atoi(id));
				strcpy(register_data[4], timestamp);
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				/*Wait for overtime answer from the server with the specific id*/
				MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);

			}else if(strcmp(command, "SYNC") == 0){
				/*Coordinator must proceed to sync after all registrations have ended*/
				while(registers!=0){
					MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
					registers--;
				}

				printf("Cordinator starting SYNC\n");

				/*Send sync to the server leader*/
				sprintf(register_data[0],"%d", SYNC);
				sprintf(register_data[2],"%d", rank);
				sprintf(register_data[3],"%d", server_leader);
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				/*Wait for sync answer from the server leader*/
				MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);

			}else if(strcmp(command, "PRINT") == 0){

				/*Send print to the server leader*/
				sprintf(register_data[0],"%d", PRINT);
				sprintf(register_data[2],"%d", rank);
				sprintf(register_data[3],"%d", server_leader);
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, server_leader, TAG, MPI_COMM_WORLD);

				/*Wait for print answer from the server leader*/
				MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);

				/*Send print to the client leader*/
				sprintf(register_data[0],"%d", PRINT);
				sprintf(register_data[2],"%d", rank);
				sprintf(register_data[3],"%d", client_leader);
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, client_leader, TAG, MPI_COMM_WORLD);

				/*Wait for print answer from the client leader*/
				MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);

			}else if(strcmp(command, "START_LEADER_ELECTION_CLIENTS") == 0){

				printf("Client leader election started\n");
				/*Send message to all clients to start server leader election*/
				connect_data[0]=START_LEADER_ELECTION_CL;
				for(int i=num_servers*num_servers+1; i<process_num; i++){
					MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, i, TAG, MPI_COMM_WORLD);
				}

				/*Wait from client leader to send message and update the var*/
				for(int i=num_servers*num_servers+1; i<process_num; i++){
					MPI_Recv(&connect_data, 1, ELECT_CLIENTS_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
				}
				client_leader = connect_data[1];
				printf("Coordinator received %d as client leader\n", connect_data[1]);

				/*Inform clients about server leader*/
				int tmp=client_leader;
				for(int i=1; i<=num_servers*num_servers; i++){
					MPI_Send(&client_leader, 1, MPI_INT, i, TAG, MPI_COMM_WORLD);
				}
				/*Inform servers about client leader*/
				tmp=server_leader;
				for(int i=num_servers*num_servers+1; i<process_num; i++){
					MPI_Send(&server_leader, 1, MPI_INT, i, TAG, MPI_COMM_WORLD);
				}

				/*Get answer from clients about server leader*/
				for(int i=1; i<=num_servers*num_servers; i++){
					MPI_Recv(&tmp, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
				}
				/*Get answer from servers about client leader*/
				for(int i=num_servers*num_servers+1; i<process_num; i++){
					MPI_Recv(&tmp, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
				}
			}else if(strcmp(command, "START_LEADER_ELECTION_SERVERS") == 0){
				/*Send message to all clients to make clear that connection has finished an wait until all of them answer*/
				connect_data[0]=CONN_END_M;
				for(int i=num_servers*num_servers+1; i<process_num; i++){
					MPI_Send(&connect_data, 1, CONN_MESSAGE, i, TAG, MPI_COMM_WORLD);
				}

				for(int i=num_servers*num_servers+1; i<process_num; i++){
					MPI_Recv(&connect_data, 1, CONN_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
				}
				/*Send message to every server to start server leader election*/
				connect_data[0]=START_LEADER_ELECTION_SE;
				for(int i=1; i<=num_servers*num_servers; i++){
					MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, i, TAG, MPI_COMM_WORLD);
				}
				/*When server leader sends back update var*/
				for(int i=1; i<=num_servers*num_servers; i++){
					MPI_Recv(&connect_data, 1, ELECT_SERVERS_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
				}
				printf("Coordinator received %d as server leader\n", connect_data[1]);
				server_leader = connect_data[1];

			}
		}

		/*Inform clients about end of registrations*/
		sprintf(register_data[0],"%d", REGISTER_END);
		for(int i=1; i<=num_servers*num_servers; i++){
			MPI_Send(&register_data, 6, REGISTER_MESSAGE, i, TAG, MPI_COMM_WORLD);
		}
		/*Inform servers about end of registrations*/
		for(int i=num_servers*num_servers+1; i<process_num; i++){
			MPI_Send(&register_data, 6, REGISTER_MESSAGE, i, TAG, MPI_COMM_WORLD);
		}

		fclose(fp);
	/*Process is a server*/
	}else if(rank <= num_servers*num_servers){
		/*Determine neighbors of this server in torus*/
		int torus_line = ((rank-1) / num_servers) + 1;
		int torus_position = ((rank-1) % num_servers) + 1;
		int neighbors[4];
		int next_neighbor;

		//printf("[SERVER %d] Belongs to line:%d and position:%d\n", rank, torus_line, torus_position);

		if(torus_position==num_servers){
			neighbors[RIGHT]=rank-(num_servers-1);
		}else{
			neighbors[RIGHT]=rank+1;
		}

		if(torus_position==1){
			neighbors[LEFT]=rank+(num_servers-1);
		}else{
			neighbors[LEFT]=rank-1;
		}

		if(torus_line==num_servers){
			neighbors[DOWN]=torus_position;
		}else{
			neighbors[DOWN]=torus_line*num_servers+torus_position;
		}

		if(torus_line==1){
				neighbors[UP]=(num_servers-1)*num_servers+torus_position;
		}else{
				neighbors[UP]=(torus_line-2)*num_servers+torus_position;
        }

		//printf("[SERVER %d] UP:%d DOWN:%d LEFT:%d RIGHT:%d\n", rank, up_neighbor, down_neighbor, left_neighbor, right_neighbor);

		if(torus_line%2==1){
			if(torus_position==num_servers){
				if(torus_line==num_servers){
					next_neighbor=1;
				}else{
					next_neighbor=rank+num_servers;
				}
			}else{
				next_neighbor=rank+1;
			}
		}else{
			if(torus_position==1){
				if(torus_line==num_servers){
					next_neighbor=1;
				}else{
					next_neighbor=rank+num_servers;
				}
			}else{
				next_neighbor=rank-1;
			}
		}
		//printf("[SERVER %d] Has next neighbor:%d\n", rank, next_neighbor);

		/*LEADER ELECTION*/
		int parent = -1;
		int leader = -1;
		struct neighbor* children = NULL;

		struct neighbor* unexplored = make_unique_list(neighbors);
		struct neighbor* curr_unexplored;
		// printf("[SERVER %d] Has unexplored:",rank);
		curr_unexplored = unexplored;
		while(curr_unexplored!=NULL){
			// printf("%d, ", curr_unexplored->id);
			curr_unexplored = curr_unexplored->next;
		}
		// printf("\n");
		while(1){
			// printf("[SERVER %d] is waiting ...\n", rank);
			MPI_Recv(&connect_data, 1, ELECT_SERVERS_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
			if(connect_data[0]==START_LEADER_ELECTION_SE){
				// printf("[SERVER %d] Started server election\n",rank);
				if(parent==-1){
					leader = rank;
					parent = rank;

					if(unexplored!=NULL){
						connect_data[0] = LEADER;
						connect_data[1] = leader;
						connect_data[2] = rank;
						connect_data[3] = unexplored->id;
						unexplored = unexplored->next;
						MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						// printf("[SERVER %d] Has sent <LEADER> to %d with leader %d\n",rank, connect_data[3], connect_data[1]);
					}else{
						if(parent!=rank){
							connect_data[0] = PARENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = parent;
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] Has sent <PARENT> to %d with leader %d\n",rank,connect_data[3], connect_data[1]);
						}else{
							// printf("---[SERVER %d] IS THE ROOT %d---\n",rank, leader);
							struct neighbor* curr_child = children;
							while(curr_child!=NULL){
								connect_data[0] = LEADER_ANNOUNCEMENT;
								connect_data[1] = leader;
								connect_data[2] = rank;
								connect_data[3] = curr_child->id;
								// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
								MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
								curr_child=curr_child->next;
							}
							connect_data[0] = LEADER_ANNOUNCEMENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = 0;
							// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] has children: ",rank);
							curr_child = children;
							while(curr_child!=NULL){
								// printf("%d, ",curr_child->id);
								curr_child=curr_child->next;
							}
							// printf("\n[SERVER %d] Exiting\n",rank);
							break;
						}
					}
				}
			}else if(connect_data[0]==LEADER){
				// printf("[SERVER %d] Received <LEADER> is %d from %d\n",rank, connect_data[1], connect_data[2]);
				if(leader<connect_data[1]){
					leader = connect_data[1];
					parent = connect_data[2];
					children = NULL;
					unexplored = make_unique_list(neighbors);
					unexplored = delete_from_list(unexplored, connect_data[2]);

					if(unexplored!=NULL){
						connect_data[0] = LEADER;
						connect_data[1] = leader;
						connect_data[2] = rank;
						connect_data[3] = unexplored->id;
						unexplored = unexplored->next;
						MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						// printf("[SERVER %d] Has sent <LEADER> to %d with leader %d\n",rank, connect_data[3], connect_data[1]);
					}else{
						if(parent!=rank){
							connect_data[0] = PARENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = parent;
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] Has sent <PARENT> to %d with leader %d\n",rank,connect_data[3], connect_data[1]);
						}else{
							// printf("---[SERVER %d] IS THE ROOT %d---\n",rank, leader);
							struct neighbor* curr_child = children;
							while(curr_child!=NULL){
								connect_data[0] = LEADER_ANNOUNCEMENT;
								connect_data[1] = leader;
								connect_data[2] = rank;
								connect_data[3] = curr_child->id;
								// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
								MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
								curr_child=curr_child->next;
                            }
							connect_data[0] = LEADER_ANNOUNCEMENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = 0;
							// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] has children: ",rank);
							curr_child = children;
							while(curr_child!=NULL){
								// printf("%d, \n",curr_child->id);
								curr_child=curr_child->next;
							}
							// printf("\n[SERVER %d] Exiting\n",rank);
							break;
						}
					}
				}else if(leader==connect_data[1]){
					connect_data[0] = ALREADY;
					connect_data[3] = connect_data[2];
					connect_data[2] = rank;
					MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
					// printf("[SERVER %d] Sends <ALREADY> to %d with leader %d\n", rank, connect_data[3], connect_data[1]);
				}
			}else if(connect_data[0]==ALREADY){
				// printf("[SERVER %d] Received <ALREADY> from %d with leader %d\n",rank, connect_data[2], connect_data[1]);
				if(connect_data[1]==leader){
					if(unexplored!=NULL){
						// printf("[SERVER %d] has unexplored: %d\n", rank, unexplored->id);
						connect_data[0] = LEADER;
						connect_data[1] = leader;
						connect_data[2] = rank;
						connect_data[3] = unexplored->id;
						unexplored = unexplored->next;
						MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						// printf("[SERVER %d] Has sent <LEADER> to %d with leader %d\n",rank, connect_data[3], connect_data[1]);
					}else{
						if(parent!=rank){
							connect_data[0] = PARENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = parent;
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] Has sent <PARENT> to %d with leader %d\n",rank,connect_data[3], connect_data[1]);
						}else{
							// printf("---[SERVER %d] IS THE ROOT %d---\n",rank, leader);
							struct neighbor* curr_child = children;
							while(curr_child!=NULL){
								connect_data[0] = LEADER_ANNOUNCEMENT;
								connect_data[1] = leader;
								connect_data[2] = rank;
								connect_data[3] = curr_child->id;
								// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
								MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);

								curr_child=curr_child->next;
							}
							connect_data[0] = LEADER_ANNOUNCEMENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = 0;
							// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] has children: ",rank);
							curr_child = children;
							while(curr_child!=NULL){
								// printf("%d, \n",curr_child->id);
								curr_child=curr_child->next;
							}
							// printf("\n[SERVER %d] Exiting\n",rank);
							break;
						}
					}
				}
			}else if(connect_data[0]==PARENT){
				// printf("[SERVER %d] Received <PARENT> from %d with leader %d\n",rank, connect_data[2], connect_data[1]);
				if(connect_data[1]==leader){
					children = push_to_list(children, connect_data[2]);
					if(unexplored!=NULL){
						connect_data[0] = LEADER;
						connect_data[1] = leader;
						connect_data[2] = rank;
						connect_data[3] = unexplored->id;
						unexplored = unexplored->next;
						MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						// printf("[SERVER %d] Has sent <LEADER> to %d with leader %d\n",rank, connect_data[3], connect_data[1]);
					}else{
						if(parent!=rank){
							connect_data[0] = PARENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = parent;
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] Has sent <PARENT> to %d with leader %d\n",rank,connect_data[3], connect_data[1]);
                        }else{
							// printf("---[SERVER %d] IS THE ROOT %d---\n",rank, leader);
							struct neighbor* curr_child = children;
							while(curr_child!=NULL){
								connect_data[0] = LEADER_ANNOUNCEMENT;
								connect_data[1] = leader;
								connect_data[2] = rank;
								connect_data[3] = curr_child->id;
								// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
								MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
								curr_child=curr_child->next;
                            }
							connect_data[0] = LEADER_ANNOUNCEMENT;
							connect_data[1] = leader;
							connect_data[2] = rank;
							connect_data[3] = 0;
							// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
							MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							// printf("[SERVER %d] has children: ",rank);
							curr_child = children;
							while(curr_child!=NULL){
								// printf("%d, ",curr_child->id);
								curr_child=curr_child->next;
							}
							// printf("\n[SERVER %d] Exiting\n",rank);
							break;
						}
					}
				}
			}else if(connect_data[0]==LEADER_ANNOUNCEMENT){
				// printf("[SERVER %d] Recognised %d as root from %d\n",rank, leader, connect_data[2]);
				struct neighbor* curr_child = children;
				while(curr_child!=NULL){
					connect_data[0] = LEADER_ANNOUNCEMENT;
					connect_data[1] = leader;
					connect_data[2] = rank;
					connect_data[3] = curr_child->id;
					MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
					// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
					curr_child=curr_child->next;
				}
				connect_data[0] = LEADER_ANNOUNCEMENT;
				connect_data[1] = leader;
				connect_data[2] = rank;
				connect_data[3] = 0;
				// printf("[SERVER %d] Has sent to %d leader announcement with leader %d\n",rank, connect_data[3], leader);
				MPI_Send(&connect_data, 1, ELECT_SERVERS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
				// printf("[SERVER %d] has children: ",rank);
				curr_child = children;
				while(curr_child!=NULL){
					// printf("%d, ",curr_child->id);
					curr_child=curr_child->next;
				}
				// printf("\n[SERVER %d] Exiting\n",rank);
                break;
			}
		}

		/*Update client and server leaders after elections*/
		MPI_Recv(&client_leader, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
		server_leader=leader;
		MPI_Send(&client_leader, 1, MPI_INT, connect_data[3], TAG, MPI_COMM_WORLD);
		// printf("[SERVER %d] Got %d as client leader\n", rank, client_leader);

		/*REGISTRATIONS*/
		struct registration* registrations=NULL;
		int register_cnt;
		if(server_leader==rank){
			register_cnt=0;
		}else{
			register_cnt=-1;
		}
		int s = -1;
		char total_time[15];
		strcpy(total_time,"0:0:0");
		int total_hours=0;
		int total_minutes=0;
		int total_seconds=0;
		char tmp_total_time[15];
		strcpy(tmp_total_time,"0:0:0");
		int tmp_total_hours=0;
		int tmp_total_minutes=0;
		int tmp_total_seconds=0;
		int total_requests = 0;
		int tmp_total_requests=0;
		int synced_servers = 0;
		int print_servers = 0;
		int registers_with_timestamp=0;
		struct registration* curr_registration=NULL;
		while(1){
			MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
			if(atoi(register_data[0])==REGISTER_M){
				if(server_leader==rank && atoi(register_data[2])==client_leader){
					// register_cnt++;
					// s = register_cnt%(num_servers*num_servers) + 1;
					s = atoi(register_data[1])%(num_servers*num_servers) + 1;
					sprintf(register_data[3],"%d", s);
				}
				if(atoi(register_data[3])!=rank){
					sprintf(register_data[2],"%d", rank);
					// printf("[SERVER %d] Received register type %s at %s\n", rank, register_data[4], register_data[5]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, next_neighbor, TAG, MPI_COMM_WORLD);
				}else{
					// printf("[SERVER %d] Stored register type %s at %s\n", rank, register_data[4], register_data[5]);
					registrations = push_registration(registrations, atoi(register_data[1]), register_data[4], register_data[5]);
					sprintf(register_data[0],"%d", ACK_REGISTER);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[1]), TAG, MPI_COMM_WORLD);
				}
			}else if(atoi(register_data[0])==SYNC){
				struct neighbor* curr_child = children;
				// printf("[SERVER %d] Received sync from %s\n",rank, register_data[2]);
				while(curr_child!=NULL){
					sprintf(register_data[0],"%d", SYNC);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", curr_child->id);
					// printf("[SERVER %d] Sending sync to %s\n",rank, register_data[3]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, curr_child->id, TAG, MPI_COMM_WORLD);
					curr_child=curr_child->next;
				}
				strcpy(total_time,"0:0:0");
				total_requests=0;
				if(rank!=server_leader){
					strcpy(total_time,calculate_registrations(registrations, total_time, &total_requests));
					sprintf(register_data[0],"%d", SYNC_ACK);
					sprintf(register_data[1],"%d", rank);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					strcpy(register_data[4], total_time);
					sprintf(register_data[5],"%d", total_requests);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					// printf("[SERVER %d] Sending sync ack to %s\n",rank, register_data[3]);
					// printf("[SERVER %d] has registrations:\n",rank);
					// curr_registration = registrations;
					// 	while(curr_registration!=NULL){
					// 		printf("[SERVER %d] %d %s %s\n",rank, curr_registration->client_id, curr_registration->type, curr_registration->timestamp);
					// 		curr_registration=curr_registration->next;
					// 	}
				}else{
					synced_servers=0;
					strcpy(tmp_total_time,"0:0:0");
					tmp_total_requests=0;
				}
			}else if(atoi(register_data[0])==SYNC_ACK){
				// printf("[SERVER %d] Received sync ack from %s\n",rank, register_data[2]);
				if(rank==server_leader){
					synced_servers++;
					total_requests+= atoi(register_data[5]);

					total_hours=atoi(strtok(total_time,":"));
					total_minutes=atoi(strtok(NULL,":"));
					total_seconds=atoi(strtok(NULL,":"));

					tmp_total_hours=atoi(strtok(register_data[4],":"));
					tmp_total_minutes=atoi(strtok(NULL,":"));
					tmp_total_seconds=atoi(strtok(NULL,":"));

					increment_time(&total_hours, &total_minutes, &total_seconds, tmp_total_hours, tmp_total_minutes, tmp_total_seconds);

					sprintf(total_time, "%d:%d:%d",total_hours, total_minutes,total_seconds);
					// printf("LEADER has %s\n",total_time);
					strcpy(tmp_total_time,"0:0:0");
					if(synced_servers==num_servers*num_servers-1){

						strcpy(tmp_total_time, calculate_registrations(registrations, tmp_total_time, &tmp_total_requests));
						total_requests+= tmp_total_requests;

						total_hours=atoi(strtok(total_time,":"));
						total_minutes=atoi(strtok(NULL,":"));
						total_seconds=atoi(strtok(NULL,":"));

						tmp_total_hours=atoi(strtok(tmp_total_time,":"));
						tmp_total_minutes=atoi(strtok(NULL,":"));
						tmp_total_seconds=atoi(strtok(NULL,":"));

						increment_time(&total_hours, &total_minutes, &total_seconds, tmp_total_hours, tmp_total_minutes, tmp_total_seconds);
						sprintf(total_time, "%d:%d:%d",total_hours, total_minutes,total_seconds);

						sprintf(register_data[0],"%d", SYNC_ACK);
						sprintf(register_data[2],"%d", rank);
						strcpy(register_data[3],"0");
						strcpy(register_data[4], total_time);
						sprintf(register_data[5],"%d", total_requests);
						// printf("[SERVER %d] Sending sync ack to coordinator\n",rank);
						printf("LEADER SYNC OK\n");
						// printf("[SERVER %d] has registrations:\n",rank);
						// curr_registration = registrations;
						// while(curr_registration!=NULL){
						// 	printf("[SERVER %d] %d %s %s\n",rank, curr_registration->client_id, curr_registration->type, curr_registration->timestamp);
						// 	curr_registration=curr_registration->next;
						// }
						MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					}
				}else{
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					// printf("[SERVER %d] Sending sync ack to %s\n",rank, register_data[3]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				}
			}else if(atoi(register_data[0])==OVERTIME){
				registers_with_timestamp = 0;
				sprintf(register_data[0],"%d", OVERTIME_ACK);
				sprintf(register_data[2],"%d", rank);
				sprintf(register_data[3],"%d", next_neighbor);
				sprintf(register_data[5],"%d", registers_with_timestamp);
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
			}else if(atoi(register_data[0])==OVERTIME_ACK){
				registers_with_timestamp = atoi(register_data[5]);
				if(rank==atoi(register_data[1])){
					registers_with_timestamp +=	find_registers_with_timestamp(registrations,register_data[4]);
					sprintf(register_data[0],"%d", OVERTIME_ACK);
					sprintf(register_data[2],"%d", rank);
					strcpy(register_data[3],"0");
					sprintf(register_data[5],"%d", registers_with_timestamp);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					printf("SERVER %s COUNTED %s OVERTIMES\n",register_data[1], register_data[5]);
				}else{
					registers_with_timestamp = atoi(register_data[5]);
					registers_with_timestamp +=	find_registers_with_timestamp(registrations,register_data[4]);
					sprintf(register_data[0],"%d", OVERTIME_ACK);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", next_neighbor);
					sprintf(register_data[5],"%d", registers_with_timestamp);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				}
			}else if(atoi(register_data[0])==PRINT){
				struct neighbor* curr_child = children;
				while(curr_child!=NULL){
					sprintf(register_data[0],"%d", PRINT);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", curr_child->id);
					// printf("[SERVER %d] Sending print to %s\n",rank, register_data[3]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, curr_child->id, TAG, MPI_COMM_WORLD);
					curr_child=curr_child->next;
				}

				if(rank!=server_leader){
					printf("SERVER %d HAS %d RECORDS WITH %s HOURS\n",rank, total_requests, total_time);
					sprintf(register_data[0],"%d", PRINT_ACK);
					sprintf(register_data[1],"%d", rank);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					sprintf(register_data[4],"%d", total_hours);
					sprintf(register_data[5],"%d", total_requests);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					// printf("[SERVER %d] Sending print ack to %s\n",rank, register_data[3]);
				}else{
					print_servers = 0;
				}
			}else if(atoi(register_data[0])==PRINT_ACK){
				// printf("[SERVER %d] Received print ack from %s\n",rank, register_data[2]);
				if(rank==server_leader){
					print_servers++;
					if(print_servers==num_servers*num_servers-1){
						// strcpy(tmp_total_time,"0:0:0");
						// tmp_total_requests = 0;
						// strcpy(tmp_total_time,calculate_registrations(registrations, tmp_total_time, &tmp_total_requests));
						// total_hours= tmp_total_hours;
						// total_requests= tmp_total_requests;
						printf("LEADER SERVER %d HAS %d RECORDS WITH %s HOURS\n",rank, total_requests, total_time);
						sprintf(register_data[0],"%d", PRINT_ACK);
						sprintf(register_data[2],"%d", rank);
						strcpy(register_data[3],"0");
						sprintf(register_data[4],"%d", total_hours);
						sprintf(register_data[5],"%d", total_requests);
						// printf("[SERVER %d] Sending sync ack to coordinator\n",rank);
						MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					}
				}else{
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					// printf("[SERVER %d] Sending print ack to %s\n",rank, register_data[3]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				}
			}else if(atoi(register_data[0])==REGISTER_END){
				break;
			}
		}
	/*Process is a client*/
	}else{
		/*CONNECTIONS*/
		// printf("[CLIENT %d] STARTED\n", rank);

		struct neighbor* neighbors = NULL;
		int neighbors_length = 0;
		while(1){
			MPI_Recv(&connect_data, 1, CONN_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
			/*Then connect message has come from coordinator*/
			if(connect_data[0]==CONNECT_M){

				// printf("[CLIENT %d] received connection message from coordinator\n", rank);
				connect_data[0] = NEIGHBOR_M;
				MPI_Send(&connect_data, 1, CONN_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
				// printf("[CLIENT %d] sent neighbor message to %d\n", rank, connect_data[3]);

				MPI_Recv(&connect_data, 1, CONN_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
				// printf("[CLIENT %d] received ack message from neighbor %d\n", rank, connect_data[3]);
				MPI_Send(&connect_data, 1, CONN_MESSAGE, connect_data[1], TAG, MPI_COMM_WORLD);
				// printf("[CLIENT %d] sent ack message to coordinator\n", rank, connect_data[2]);
				neighbors = push_to_list(neighbors,connect_data[3]);
				neighbors_length++;
			}else if(connect_data[0] == NEIGHBOR_M){

				// printf("[CLIENT %d] received neighbor message from neighbor %d\n", rank, connect_data[2]);
				connect_data[0] = ACK_M;
				MPI_Send(&connect_data, 1, CONN_MESSAGE, connect_data[2], TAG, MPI_COMM_WORLD);
				// printf("[CLIENT %d] sent ack message to %d\n", rank, connect_data[2]);
				neighbors = push_to_list(neighbors,connect_data[2]);
				neighbors_length++;
			}else{
				// printf("[CLIENT %d] %d\n",rank, connect_data[0]);
				// printf("[CLIENT %d] Neighbors: ",rank);
				struct neighbor* curr_neighbor = neighbors;
				while(curr_neighbor!=NULL){
					// printf("%d, ",curr_neighbor->id);
					curr_neighbor = curr_neighbor->next;
				}
				// printf("\n");
				// printf("[CLIENT %d] Exited connecting state\n",rank);
				connect_data[0] = ACK_M;
				connect_data[2] = rank;
				connect_data[3] = 0;
                MPI_Send(&connect_data, 1, CONN_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
				break;
			}
		}


		/*LEADER ELECTION*/

		int leader = -1;
		int parent = -1;
		struct neighbor* neighbors_elect = copy_list(neighbors);
		int neighbors_elect_length = neighbors_length;
		int last_elect_neighbor = -1;
		struct neighbor* curr_neighbor = NULL;
		struct neighbor* children = NULL;
		while(1){
			//printf("[CLIENT %d] is waiting..\n",rank);
			MPI_Recv(&connect_data, 1, ELECT_CLIENTS_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
			//printf("[CLIENT %d] has received %d\n",rank, connect_data[0]);
			if(connect_data[0]==START_LEADER_ELECTION_CL){
				if(neighbors_elect_length==1){ //if this node is a leaf
					//printf("[CLIENT %d] Starts leader election\n", rank);
					// printf("[CLIENT %d] sends <ELECT> to %d\n", rank, neighbors_elect->id);
					connect_data[0]=ELECT;
					connect_data[2]=rank;
					connect_data[3]=neighbors_elect->id;
					neighbors_elect = delete_from_list(neighbors_elect, neighbors_elect->id);
                                        neighbors_elect_length--;
					MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
				}
				/*else if(neighbors_elect_length==0){
					break;
				}*/

			}else if(connect_data[0]==ELECT){
				// printf("[CLIENT %d] Received <ELECT> from %d\n",rank, connect_data[2]);
				if(neighbors_elect_length==0 && last_elect_neighbor!=-1){
					if(rank>last_elect_neighbor){
						// printf("---[CLIENT %d] IS THE LEADER---\n", rank);
						curr_neighbor = neighbors;
						leader = rank;
						parent = rank;
						while(curr_neighbor!=NULL){
							connect_data[0]=LEADER_ELECTION_CLIENT_DONE;
							connect_data[1]=rank;
							connect_data[2]=rank;
							connect_data[3]=curr_neighbor->id;
							MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							children = push_to_list(children, curr_neighbor->id);
							curr_neighbor=curr_neighbor->next;
						}

						connect_data[0]=LEADER_ELECTION_CLIENT_DONE;
						connect_data[1]=rank;
						connect_data[2]=rank;
						connect_data[3]=0;
						MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						// printf("[CLIENT %d] Exited\n",rank);
						// printf("[CLIENT %d] has parent:%d\n",rank, parent);
						// printf("[CLIENT %d] has children: ");
						struct neighbor* curr_children = children;
						while(curr_children!=NULL){
							// printf("%d,", curr_children->id);
							curr_children = curr_children->next;
						}
						// printf("\n");
						break;
					}
				}else{
					neighbors_elect = delete_from_list(neighbors_elect, connect_data[2]);
					neighbors_elect_length--;
					//printf("[CLIENT %d] has %d neighbors left\n",rank,neighbors_elect_length);
					if(neighbors_elect_length==1){
						connect_data[0]=ELECT;
						connect_data[2]=rank;
						connect_data[3]=neighbors_elect->id;
						last_elect_neighbor = neighbors_elect->id;
						neighbors_elect = delete_from_list(neighbors_elect, last_elect_neighbor);
						neighbors_elect_length--;
						// printf("[CLIENT %d] sends <ELECT> to %d\n", rank, connect_data[3]);
						MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
					}else if(neighbors_elect_length==0){
						// printf("--[CLIENT %d] IS THE LEADER--\n", rank);
						leader = rank;
						parent = rank;
						curr_neighbor = neighbors;
						while(curr_neighbor!=NULL){
							connect_data[0]=LEADER_ELECTION_CLIENT_DONE;
							connect_data[1]=rank;
							connect_data[2]=rank;
							connect_data[3]=curr_neighbor->id;
							MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
							children = push_to_list(children, curr_neighbor->id);
							curr_neighbor=curr_neighbor->next;
                        }

						connect_data[0]=LEADER_ELECTION_CLIENT_DONE;
						connect_data[1]=rank;
						connect_data[2]=rank;
						connect_data[3]=0;
						MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						// printf("[CLIENT %d] Exited\n",rank);
						// printf("[CLIENT %d] has parent:%d\n",rank, parent);
						// printf("[CLIENT %d] has children: ");
						struct neighbor* curr_children = children;
						while(curr_children!=NULL){
							// printf("%d,", curr_children->id);
							curr_children = curr_children->next;
						}
						// printf("\n");
						break;
					}
				}
			}else if(connect_data[0]==LEADER_ELECTION_CLIENT_DONE){
				// printf("[CLIENT %d] recognised %d as leader\n",rank, connect_data[1]);
				leader = connect_data[1];
				int neighbor_sender = connect_data[2];
				parent = neighbor_sender;
				curr_neighbor = neighbors;
				while(curr_neighbor!=NULL){
					if(curr_neighbor->id!=neighbor_sender){
						// printf("[CLIENT %d] sending leader to %d\n", rank, curr_neighbor->id);
						connect_data[0]=LEADER_ELECTION_CLIENT_DONE;
						connect_data[1]=leader;
						connect_data[2]=rank;
						connect_data[3]=curr_neighbor->id;
						MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
						children = push_to_list(children, curr_neighbor->id);
					}
					curr_neighbor=curr_neighbor->next;
				}
				connect_data[0]=LEADER_ELECTION_CLIENT_DONE;
				connect_data[1]=leader;
				connect_data[2]=rank;
				connect_data[3]=0;
				MPI_Send(&connect_data, 1, ELECT_CLIENTS_MESSAGE, connect_data[3], TAG, MPI_COMM_WORLD);
				// printf("[CLIENT %d] Exited\n",rank);
				// printf("[CLIENT %d] has parent:%d\n",rank, parent);
				// printf("[CLIENT %d] has children: ");
				struct neighbor* curr_children = children;
				while(curr_children!=NULL){
					// printf("%d,", curr_children->id);
					curr_children = curr_children->next;
				}
				// printf("\n");
				break;
			}
		}

		/*Update client and server leaders after elections*/
		MPI_Recv(&server_leader, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
		client_leader=leader;
		MPI_Send(&server_leader, 1, MPI_INT, connect_data[3], TAG, MPI_COMM_WORLD);

		// printf("[CLIENT %d] Got %d as server leader\n", rank, server_leader);

		/*REGISTRATIONS*/
		int active_requests = 0;
		int request_count=0;
		int print_clients = 0;
		while(1){
			MPI_Recv(&register_data, 6, REGISTER_MESSAGE, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
			if(atoi(register_data[0])==REGISTER_M){
				if(atoi(register_data[1])==rank){
					active_requests++;
					request_count++;
				}
				if(client_leader==rank){
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", server_leader);
					// printf("[CLIENT %d] Received register type %s at %s\n", rank, register_data[4], register_data[5]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				}else{
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					// printf("[CLIENT %d] Received register type %s at %s\n", rank, register_data[4], register_data[5]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				}
			}else if(atoi(register_data[0])==ACK_REGISTER){
				printf("CLIENT %d REGISTERED %s %s\n",rank, register_data[4], register_data[5]);
				sprintf(register_data[2],"%d", rank);
				strcpy(register_data[3],"0");
				MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				active_requests--;
			}else if(atoi(register_data[0])==PRINT){
				struct neighbor* curr_child = children;
				while(curr_child!=NULL){
					sprintf(register_data[0],"%d", PRINT);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", curr_child->id);
					// printf("[CLIENT %d] Sending print to %s\n",rank, register_data[3]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, curr_child->id, TAG, MPI_COMM_WORLD);
					curr_child=curr_child->next;
				}

				if(rank!=client_leader){
					printf("CLIENT %d PROCESSED %d REQUESTS\n",rank, request_count);
					sprintf(register_data[0],"%d", PRINT_ACK);
					sprintf(register_data[1],"%d", rank);
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					// printf("[SERVER %d] Sending print ack to %s\n",rank, register_data[3]);
				}else{
					print_clients = 0;
				}
			}else if(atoi(register_data[0])==PRINT_ACK){
				// printf("[SERVER %d] Received print ack from %s\n",rank, register_data[2]);
				if(rank==client_leader){
					print_clients++;
					if(print_clients == process_num - num_servers*num_servers-2){
						printf("LEADER CLIENT %d PROCESSED %d REQUESTS\n",rank, request_count);
						sprintf(register_data[0],"%d", PRINT_ACK);
						sprintf(register_data[2],"%d", rank);
						strcpy(register_data[3],"0");
						// printf("[SERVER %d] Sending sync ack to coordinator\n",rank);
						MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
					}
				}else{
					sprintf(register_data[2],"%d", rank);
					sprintf(register_data[3],"%d", parent);
					// printf("[SERVER %d] Sending print ack to %s\n",rank, register_data[3]);
					MPI_Send(&register_data, 6, REGISTER_MESSAGE, atoi(register_data[3]), TAG, MPI_COMM_WORLD);
				}
			}else if(atoi(register_data[0])==REGISTER_END){
				break;
			}
		}
	}

	MPI_Finalize();
}

struct neighbor* push_to_list(struct neighbor* list, int id){
	struct neighbor* newNode = (struct neighbor*) malloc(sizeof(struct neighbor));
	newNode->id = id;
	newNode->next = list;
	return newNode;
}

struct neighbor* copy_list(struct neighbor* list){
	struct neighbor* copied_list=NULL;
	struct neighbor* curr = list;
	struct neighbor* curr_copied = NULL;

	copied_list = (struct neighbor*) malloc(sizeof(struct neighbor));
	copied_list->id = curr->id;
	curr=curr->next;
	curr_copied = copied_list;
	while(curr!=NULL){
		curr_copied->next = (struct neighbor*) malloc(sizeof(struct neighbor));
        curr_copied->next->id = curr->id;
		curr_copied=curr_copied->next;
		curr=curr->next;
	}
	curr_copied->next=NULL;
	return copied_list;
}

struct neighbor* delete_from_list(struct neighbor* list, int id){
	struct neighbor* curr = list;
	struct neighbor* prev = list;
	if(curr->id == id){
		return curr->next;
	}
	curr= curr->next;
	while(curr!=NULL){
		if(curr->id == id){
			prev->next=curr->next;
			return list;
		}
		prev=prev->next;
		curr= curr->next;
	}
	return list;
}

struct neighbor* make_unique_list(int items[]){
	struct neighbor* list = (struct neighbor*) malloc(sizeof(struct neighbor));
	struct neighbor* curr = list;
	curr->id = items[0];
	curr->next = (struct neighbor*) malloc(sizeof(struct neighbor));
	curr = curr->next;
	if(items[0]!=items[1]){
		curr->id = items[1];
		curr->next = (struct neighbor*) malloc(sizeof(struct neighbor));
		curr = curr->next;
	}
	curr->id = items[2];
	if(items[2]!=items[3]){
		curr->next = (struct neighbor*) malloc(sizeof(struct neighbor));
		curr = curr->next;
		curr->id = items[3];
	}
	curr->next = NULL;
	return list;
}

struct registration* push_registration(struct registration* registrations, int client_id, char* type, char* timestamp){
	struct registration* new_registration = (struct registration*) malloc(sizeof(struct registration));
	new_registration->client_id = client_id;
	strcpy(new_registration->type, strdup(type));
	strcpy(new_registration->timestamp, strdup(timestamp));
	new_registration->next = registrations;
	registrations = new_registration;
	return registrations;
}

struct registration* copy_registration(int client_id, char* type, char* timestamp){
	struct registration* newNode=(struct registration*) malloc(sizeof(struct registration));
	newNode->client_id=client_id;
	strncpy(newNode->type, type, 5);
	strncpy(newNode->timestamp, timestamp, 30);
	newNode->delete=0;
	newNode->next=NULL;
}

void subtract_time(char time1[], char time2[], int* hours, int* minutes, int* seconds){
	int time1_hours=atoi(strtok(time1,":"));
	int time1_minutes=atoi(strtok(NULL,":"));
	int time1_seconds=atoi(strtok(NULL,":"));
	int time2_hours=atoi(strtok(time2,":"));
	int time2_minutes=atoi(strtok(NULL,":"));
	int time2_seconds=atoi(strtok(NULL,":"));

	int kratoumeno_minutes=0;
	int kratoumeno_hours=0;
	int result_hours = 0;
	int result_minutes = 0;
	int result_seconds = 0;
	if(time2_seconds-time1_seconds<0){
		result_seconds = 60 + time2_seconds-time1_seconds;
		kratoumeno_minutes=1;
	}else{
		result_seconds = time2_seconds-time1_seconds;
	}

	if(time2_minutes-time1_minutes-kratoumeno_minutes<0){
		result_minutes = 60 + time2_minutes-time1_minutes-kratoumeno_minutes;
		kratoumeno_hours=1;
	}else{
		result_minutes = time2_minutes-time1_minutes;
	}

	result_hours = time2_hours-time1_hours-kratoumeno_hours;
	*hours=result_hours;
	*minutes=result_minutes;
	*seconds=result_seconds;
	return;
}

void increment_time(int* hours, int* minutes, int* seconds, int hours1, int minutes1, int seconds1){
	int kratoumeno_minutes=0;
	int kratoumeno_hours=0;

	if((*seconds)+seconds1>=60){
		(*seconds)+=seconds1-60;
		kratoumeno_minutes=1;
	}else{
		(*seconds)+=seconds1;
	}

	if((*minutes)+minutes1+kratoumeno_minutes>=60){
		(*minutes)+=minutes1-60;
		kratoumeno_hours=1;
	}else{
		(*minutes)+=minutes1;
	}

	(*hours)+=hours1+kratoumeno_hours;
	return;
}

char* calculate_registrations(struct registration* registrations, char total_time[15], int* total_requests){
	struct registration* curr_registration = registrations;
	struct registration* in = (struct registration*) malloc(sizeof(struct registration));
	in->client_id=-1;
	in->next=NULL;
	struct registration* out = (struct registration*) malloc(sizeof(struct registration));
	out->client_id=-1;
	out->next=NULL;
	struct registration* curr_in = in;
	struct registration* curr_out = out;

	while(curr_registration!=NULL){
		curr_registration->delete=0;
		if(strcmp(curr_registration->type,"IN")==0){
			curr_in->next=copy_registration(curr_registration->client_id, curr_registration->type, curr_registration->timestamp);
			curr_in=curr_in->next;
		}else{
			curr_out->next=copy_registration(curr_registration->client_id, curr_registration->type, curr_registration->timestamp);
			curr_out=curr_out->next;
		}
		curr_registration=curr_registration->next;
	}
	curr_in->next=NULL;
	curr_out->next=NULL;
	curr_in=in->next;
	char* in_time=NULL;
	char* in_date = NULL;
	char* out_time=NULL;
	char* out_date = NULL;

	int total_hours=atoi(strtok(total_time,":"));
	int total_minutes=atoi(strtok(NULL,":"));
	int total_seconds=atoi(strtok(NULL,":"));

	int result_hours=0;
	int result_minutes=0;
	int result_seconds=0;

	while(curr_in!=NULL){
		curr_out=out->next;
		while(curr_out!=NULL){
			if(curr_out->client_id==curr_in->client_id && curr_out->delete==0){
				(*total_requests)++;
				curr_out->delete=1;
				in_time = strtok(curr_in->timestamp, " ");
                in_date = strtok(NULL, " ");
				out_time = strtok(curr_out->timestamp, " ");
                out_date = strtok(NULL, " ");
				subtract_time(in_time, out_time, &result_hours, &result_minutes, &result_seconds);
				// printf("%d:%d:%d\n",result_hours, result_minutes, result_seconds);
				increment_time(&total_hours, &total_minutes, &total_seconds, result_hours, result_minutes, result_seconds);
				break;
			}
			curr_out=curr_out->next;
		}
		curr_in=curr_in->next;
	}

	// while (curr_registration!=NULL){
	// 	(*total_requests)++;
	// 	curr_registration=curr_registration->next;
	// }
	sprintf(total_time, "%d:%d:%d",total_hours, total_minutes,total_seconds);
	// printf("Total: %d:%d:%d\n\n",total_hours, total_minutes, total_seconds);
	return total_time;
}

int find_registers_with_timestamp(struct registration* registrations,char* timestamp){
	struct registration* curr_registration = registrations;
	struct registration* in = (struct registration*) malloc(sizeof(struct registration));
	in->client_id=-1;
	in->next=NULL;
	struct registration* out = (struct registration*) malloc(sizeof(struct registration));
	out->client_id=-1;
	out->next=NULL;
	struct registration* curr_in = in;
	struct registration* curr_out = out;

	while(curr_registration!=NULL){
		curr_registration->delete=0;
		if(strcmp(curr_registration->type,"IN")==0){
			curr_in->next=copy_registration(curr_registration->client_id, curr_registration->type, curr_registration->timestamp);
			curr_in=curr_in->next;
		}else{
			curr_out->next=copy_registration(curr_registration->client_id, curr_registration->type, curr_registration->timestamp);
			curr_out=curr_out->next;
		}
		curr_registration=curr_registration->next;
	}
	curr_in->next=NULL;
	curr_out->next=NULL;


	int result_hours=0;
	int result_minutes=0;
	int result_seconds=0;
	int count=0;

	curr_in=in->next;
	while(curr_in!=NULL){
		curr_out=out->next;
		while(curr_out!=NULL){
			// printf("%s=", curr_in->timestamp);
			if(curr_out->client_id==curr_in->client_id && curr_out->delete==0){
				curr_out->delete=1;
				char* in_time=NULL;
				char* in_date = NULL;
				char* out_time=NULL;
				char* out_date = NULL;
				in_time = strtok(curr_in->timestamp, " ");
				in_date = strtok(NULL, " ");
				out_time = strtok(curr_out->timestamp, " ");
				out_date = strtok(NULL, " ");
				if(strncmp(timestamp, out_date,10)==0){
					subtract_time(in_time, out_time, &result_hours, &result_minutes, &result_seconds);
					if(result_hours>8 || (result_hours==8 && (result_minutes>0 || result_seconds>0))){
						count++;
					}
				}
				// printf("hahaha%d \n", strncmp(timestamp, out_date,10));
				// printf("%d %d:%d:%d ", curr_out->client_id, result_hours, result_minutes, result_seconds);
				// printf("%s\n", out_date);
				break;
			}
			curr_out=curr_out->next;
		}
		curr_in=curr_in->next;
	}

	return count;
}
