struct neighbor{
	int id;
	struct neighbor* next;
};

struct registration{
	int client_id;
	char type[5];
	char timestamp[30];
	int delete;
	struct registration* next;
};

struct neighbor* push_to_list(struct neighbor*, int);

struct neighbor* copy_list(struct neighbor*);

struct neighbor* delete_from_list(struct neighbor*, int);

struct neighbor* make_unique_list(int[]);

struct registration* push_registration(struct registration*, int, char*, char*);

struct registration* copy_registration(int, char*, char*);

void subtract_time(char[], char[], int*, int*, int*);

void increment_time(int*, int*, int*, int, int, int);

char* calculate_registrations(struct registration*, char[15], int*);

int find_registers_with_timestamp(struct registration*, char*);

void connect(int, int);

void regist(int, char*, char*);

void overtime(int, char*);

void elect_servers_leader();

void elect_clients_leader();

void sync();

void print_results();
